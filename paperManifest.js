// Vuex Module Sample
import paperManifestAPI from "../../api/paperManifestAPI.js";
import _ from "lodash";

const state = {
  isExistingManifest: false,
  customersList: [],
  customerPresets: {},
  payloadID: null,
  manifestID: null,
  selectedCustomer: "",
  selectedShortCode: "",
  selectedTitleOfficer: "",
  selectedState: "",
  selectedCounty: "",
  selectedTransType: "",
  selectedOrderType: "",
  selectedEnum: "",
  stickerInput: "",
  isManualStickerInput: false,
  isStickerValid: false,
  stickerMsg: "",
  showStickerMsg: false,
  recordingDate: null,
  recordingTime: "",
  presetsLoading: false,
  pdfUrl: "",
  doctypes: [],
  status: "",
  // initial call to createManifest returns some head Data
  headData: null,
  manifestRows: [
    {
      id: 1,
      orderNumber: null,
      conformed: false,
      certified: false,
      documentType: null,
      documentTypeID: null,
      estFee: 0, // recording fee
      estCountyTax: 0, // county fee?
      estCityTax: 0, // city fee?
      estSB2Fee: 0,
      hasConformImage: false,
      documentTypeUE: null,
      payloadID: null,
      manifestID: null,
      processingOrder: null,
      selectedDoctypeObj: null,
      currentStatusID: null,
      notes: "",
      touched: false,
      rowIndex: 0,
    },
  ],
};

const getters = {
  isSpecificPresetAvail: (state) => (preset) => {
    return _.size(state.customerPresets[preset]) > 0;
  },

  rowHasCurrentSelection: (state) => (index) => {
    return !!state.manifestRows[index].documentTypeID;
  },

  rowHasConformImage: (state) => (index) => {
    return !!state.manifestRows[index].hasConformImage;
  },

  titleOfficersList(state, getters) {
    if (getters.isSpecificPresetAvail("titleOfficers")) {
      return state.customerPresets.titleOfficers.map((officer) => {
        return {
          ...officer,
          name: `${officer.firstName} ${officer.lastName}`,
        };
      });
    }
    return [];
  },
  states(state, getters) {
    if (getters.isSpecificPresetAvail("states")) {
      return state.customerPresets.states;
    }
    return [];
  },
  counties(state, getters) {
    if (state.selectedState && getters.isSpecificPresetAvail("counties")) {
      let correctCounties = state.customerPresets.counties.filter(
        (county) => county.stateID == state.selectedState.id
      );

      return correctCounties.sort((a, b) => {
        let firstCounty = a.name.toUpperCase();
        let secondCounty = b.name.toUpperCase();
        if (firstCounty < secondCounty) return -1;
        if (firstCounty > secondCounty) return 1;
        return 0;
      });
    }
    return [];
  },

  transTypes(state, getters) {
    if (getters.isSpecificPresetAvail("transTypes")) {
      return state.customerPresets.transTypes;
    }
    return [];
  },

  orderTypes(state, getters) {
    if (getters.isSpecificPresetAvail("orderTypes")) {
      return state.customerPresets.orderTypes;
    }
    return [];
  },
  enums(state, getters) {
    if (getters.isSpecificPresetAvail("enums")) {
      return state.customerPresets.enums;
    }
    return [];
  },

  areRequiredFieldsSet(state) {
    return (
      state.selectedCustomer &&
      state.selectedTitleOfficer &&
      state.selectedState &&
      state.selectedCounty &&
      state.selectedEnum &&
      state.recordingDate
    );
  },

  isFirstRow(state) {
    return state.manifestRows.length == 1;
  },

  areOptionalFieldsSet(state, getters) {
    let conditions = [];
    conditions.push(getters.isSpecificPresetAvail("transTypes"));
    conditions.push(getters.isSpecificPresetAvail("orderTypes"));
    const isOrderTypeSelected = _.size(state.selectedOrderType) > 0;
    const isTransTypeSelected = _.size(state.selectedTransType) > 0;
    const selectedValues = [isTransTypeSelected, isOrderTypeSelected];
    return _.isEqual(conditions, selectedValues);
  },

  isTableReady(state, getters) {
    return (
      (getters.areRequiredFieldsSet && getters.areOptionalFieldsSet) ||
      state.payloadID
    );
  },

  getManifestData(state, getters) {
    const saveDraft = state.status == "D" ? true : false;
    const data = {
      head: {
        customerId: state.selectedCustomer.id,
        pdfFilename: 0,
        status: state.status,
        companyName: state.selectedCustomer.name,
        titleOfficerID: state.selectedTitleOfficer.id,
        recordingDate: state.recordingDate,
        // For sure, required by PostManifest
        inputId: state.stickerInput,
        state: state.selectedState.id,
        manifestID: state.manifestID,
        payloadID: state.payloadID,
        // end required by PostManifest
        countyID: state.selectedCounty.id,
        transType: state.selectedTransType.description,
        orderType: state.selectedOrderType.description,
        shortCode: state.selectedShortCode,
        recordingTime: state.selectedEnum.description,
        saveDraft,
      },

      rows: getters.getValidRows,
    };
    return data;
  },

  showSB2column(state) {
    return state.selectedState.id == "CA";
  },

  getValidRows(state) {
    return state.manifestRows
      .filter((row) => row.documentTypeID)
      .map((row) => {
        return {
          ...row,
          currentStatusID: state.status,
        };
      });
  },
  isReadyToSave(state, getters) {
    if (state.isManualStickerInput) {
      return (
        !!state.payloadID && state.isStickerValid && getters.rowsReadyToSave
      );
    }
    return !!state.payloadID && getters.rowsReadyToSave;
  },

  isReadyToFinalize(state, getters) {
    if (state.isManualStickerInput) {
      return (
        !!state.payloadID && state.isStickerValid && getters.rowsReadyToSave
      );
    }
    return !!state.payloadID && getters.rowsReadyToSave;
  },

  numOfValidRows(state) {
    return _.size(
      state.manifestRows.filter((row) => row.documentTypeID && !row.delete)
    );
  },
  /**
   * Check that all rows that have documentID also have a documentTypeID,
   * this is important for when the user selects a new state/county and their
   * original selections clear.
   */
  rowsReadyToSave(state) {
    return state.manifestRows
      .filter((row) => row.documentID)
      .every((row) => row.documentTypeID);
  },

  totalRows(state) {
    return _.size(state.manifestRows.filter((row) => !row.delete));
  },
  /**
   * These are rows that don't have a `delete` key with a truthy value
   */
  rowsToShow(state) {
    return state.manifestRows.filter((row) => !row.delete);
  },

  isOrderCreated(state) {
    return _.size(state.headData) > 0;
  },

  isLastRowValid(state) {
    return !!state.manifestRows[_.size(state.manifestRows) - 1].documentID;
  },
};

const actions = {
  async fetchCustomers({ commit }) {
    try {
      const { data } = await paperManifestAPI.getCustomers();
      commit("SET_CUSTOMERS_LIST", data);
    } catch (e) {
      console.error(e);
    }
  },

  async fetchPaperPresets({ state, commit }) {
    commit("SET_PRESETS_LOADING", true);
    try {
      const { data } = await paperManifestAPI.getPaperPresets(
        state.selectedShortCode
      );
      commit("SET_PRESETS_LOADING", false);
      commit("SET_CUSTOMER_PRESETS", data);
      if (data["transTypes"][0]["description"] == "Not Used") {
        commit("SET_TRANS_TYPE", data["transTypes"][0]);
      }
      if (data["orderTypes"][0]["description"] == "Not Used") {
        commit("SET_ORDER_TYPE", data["orderTypes"][0]);
      }
    } catch (e) {
      commit("SET_PRESETS_LOADING", false);
      console.error(e);
    }
  },

  selectedCustomerAction({ dispatch, commit }, customer) {
    commit("RESET_CUSTOMER_SELECTIONS");
    commit("RESET_MANIFEST_ROWS");
    commit("SET_CUSTOMER", customer);
    commit("SET_SHORT_CODE", customer.shortCode);
    dispatch("fetchPaperPresets");
  },

  selectedStateAction({ commit }, selectedState) {
    commit("SET_STATE", selectedState);
    commit("CLEAR_DOCTYPES");
    commit("SET_COUNTY", "");
  },

  selectedCountyAction({ commit }, county) {
    commit("CLEAR_DOCTYPES");
    commit("SET_COUNTY", county);
  },

  async getPaperDocTypes({ state, commit }) {
    try {
      const { data } = await paperManifestAPI.getPaperDocTypes(
        state.selectedCounty.id
      );
      commit("SET_DOCTYPES", data);
    } catch (e) {
      console.error(e);
    }
  },

  async createManifest({ state, commit, getters }) {
    const manifestData = getters.getManifestData;
    try {
      const { data } = await paperManifestAPI.createManifest(manifestData);
      commit("SET_HEAD_DATA", data.head);
      commit("UPDATE_MANIFEST_ROW", {
        ...data.rows[0],
        manifestID: data.head.manifestID,
        rowIndex: 0,
      });
      commit("SET_STICKER_INPUT", data.head.inputId);
      commit("SET_MANIFEST_ID", data.head.manifestID);
      commit("SET_PAYLOAD_ID", data.head.payloadID);
    } catch (e) {
      console.error(e);
    }
  },

  async postManifest({ commit, getters }) {
    commit("SET_STATUS", "D");
    const manifestData = getters.getManifestData;
    try {
      const { data } = await paperManifestAPI.postManifest(manifestData);
      const manifestRows = data.rows;
      for (let i = 0; i < manifestRows.length; i++) {
        commit("UPDATE_MANIFEST_ROW", {
          ...manifestRows[i],
          manifestID: data.head.manifestID,
          rowIndex: i,
        });
      }
    } catch (e) {
      console.error(e);
    }
  },

  async cancelManifest({ state, dispatch }) {
    const shortCode = state.selectedShortCode;
    const manifestID = state.manifestRows[0].manifestID;
    try {
      const { data } = await paperManifestAPI.cancelManifest(
        shortCode,
        manifestID
      );
      dispatch("resetPage");
      console.log("Cancelling Manifest request", data);
    } catch (e) {
      console.error(e);
    }
  },

  async printManifest({ commit, getters }) {
    const manifestData = getters.getManifestData;
    try {
      const { data } = await paperManifestAPI.printManifest(manifestData);
      commit("SET_PDF_URL", data.data);
    } catch (e) {
      console.error(e);
    }
  },

  async removeRow({ state, commit, getters }, index) {
    commit("UPDATE_MANIFEST_ROW", {
      ...state.manifestRows[index],
      delete: true,
    });
    const manifestData = {
      head: state.headData,
      rows: getters.getValidRows,
    };
    try {
      const { data } = await paperManifestAPI.postManifest(manifestData);
      const manifestRows = data.rows;
      for (let i = 0; i < manifestRows.length; i++) {
        commit("UPDATE_MANIFEST_ROW", { ...manifestRows[i], rowIndex: i });
      }
    } catch (e) {
      console.error(e);
    }
  },

  resetPage({ commit }) {
    commit("RESET_CUSTOMER_SELECTIONS");
    commit("SET_STICKER_INPUT", "");
    commit("SET_MANIFEST_ID", "");
    commit("RESET_MANIFEST_ROWS");
    commit("SET_HEAD_DATA", null);
    commit("SET_RECORDING_DATE", "");
    commit("SET_CUSTOMER", "");
  },

  async populateExistingManifest({ state, dispatch, commit }, data) {
    let headData = {};
    commit("SET_IS_EXISTING_MANIFEST", true);
    const selectedCustomer = data[0]["customer"];
    headData.customerId = selectedCustomer.id;
    headData.companyName = selectedCustomer.name;
    commit("SET_CUSTOMER", selectedCustomer);
    dispatch("fetchCustomers");
    const shortCode = selectedCustomer.shortCode;
    commit("SET_SHORT_CODE", shortCode);
    headData.shortCode = shortCode;
    dispatch("fetchPaperPresets");
    const stickerInput = data[1].inputId;
    commit("SET_STICKER_INPUT", stickerInput);
    headData.inputId = stickerInput;
    const manifestID = data[1]["manifestID"];
    commit("SET_MANIFEST_ID", manifestID);
    headData.manifestID = manifestID;
    const payloadID = data[2][0]["payloadID"];
    commit("SET_PAYLOAD_ID", payloadID);
    let titleOfficer = data[0]["titleOfficers"].find(
      (obj) => obj.id == data[1]["titleOfficerID"]
    );
    titleOfficer = {
      ...titleOfficer,
      name: `${titleOfficer.firstName} ${titleOfficer.lastName}`,
    };
    commit("SET_TITLE_OFFICER", titleOfficer);
    headData.titleOfficerID = titleOfficer.id;
    const recordingDate = data[1]["recordingDate"];
    commit("SET_RECORDING_DATE", recordingDate);
    headData.recordingDate = recordingDate;
    const selectedState = data[0]["states"].find(
      (state) => state.id == data[1]["stateID"]
    );
    commit("SET_STATE", selectedState);
    headData.state = selectedState.name;
    const county = data[0]["counties"].find(
      (county) => county.id == data[1]["countyID"]
    );
    commit("SET_COUNTY", county);
    headData.countyID = county.id;
    await dispatch("getPaperDocTypes");
    const selectedEnum = data[0]["enums"].find(
      (obj) => obj.description == data[1]["recordingTime"]
    );
    commit("SET_ENUM", selectedEnum);
    headData.recordingTime = selectedEnum.description;
    if (selectedCustomer["usesOrderType"] == 1) {
      const orderType = data[0]["orderTypes"].find(
        (obj) => obj.description == data[2][0]["orderType"]
      );
      commit("SET_ORDER_TYPE", orderType);
      headData.orderType = orderType.description;
    } else {
      const orderType = data[0]["orderTypes"][0];
      commit("SET_ORDER_TYPE", orderType);
      headData.orderType = orderType.description;
    }

    if (selectedCustomer["usesTransType"] == 1) {
      const transType = data[0]["transTypes"].find(
        (obj) => obj.description == data[2][0]["transType"]
      );
      commit("SET_TRANS_TYPE", transType);
      headData.transType = transType.description;
    } else {
      const transType = data[0]["transTypes"][0];
      commit("SET_TRANS_TYPE", transType);
      headData.transType = transType.description;
    }

    headData.status = "D";

    commit("SET_HEAD_DATA", headData);
    const existingManifestRows = data[2].map((row, i) => {
      const selectedDoctypeObj = state.doctypes.find(
        (obj) => obj.DisplayName == row.documentType
      );
      return {
        id: i + 1,
        orderNumber: row.orderNumber,
        certified: !!row.certified,
        conformed: !!row.conformed,
        documentID: row.documentID,
        documentType: row.documentType,
        documentTypeID: row.documentTypeID,
        estFee: row.estFee,
        estCountyTax: row.estCountyTax, // county fee?
        estCityTax: row.estCityTax, // city fee?
        estSB2Fee: row.estSB2Fee,
        hasConformImage: row.hasConformImage,
        documentTypeUE: row.documentTypeUE,
        selectedDoctypeObj,
        manifestID: row.manifestID,
        payloadID: row.payloadID,
        processingOrder: row.processingOrder,
        currentStatusID: row.currentStatusID,
        notes: row.notes,
        touched: true,
        rowIndex: i,
      };
    });
    commit("UPDATE_MANIFEST_ROWS", existingManifestRows);
  },

  updateStickerInput({ commit }, $event) {
    commit("UPDATE_MANUAL_STICKER_BOOL", true);
    commit("UPDATE_STICKER_INPUT", $event.target.value);
  },

  async validateStickerInput({ state, commit }) {
    try {
      const { data } = await paperManifestAPI.checkSticker(state.stickerInput);
      console.log("Data coming back from sticker input check", data);
      commit("UPDATE_STICKER_VALIDATION", true);
      commit("UPDATE_STICKER_MSG", "Sticker Id is valid");
      commit("UPDATE_SHOW_STICKER_MSG", true);
    } catch (e) {
      if (e.response.status == 400) {
        console.log(e.response.data.message);
        commit("UPDATE_STICKER_VALIDATION", false);
        commit("UPDATE_STICKER_MSG", e.response.data.message);
        commit("UPDATE_SHOW_STICKER_MSG", true);
        commit("UPDATE_STICKER_INPUT", "");
        commit("UPDATE_MANUAL_STICKER_BOOL", false);
      } else {
        console.error(e);
      }
    }
    setTimeout(() => {
      commit("UPDATE_STICKER_MSG", "");
      commit("UPDATE_SHOW_STICKER_MSG", false);
    }, 2500);
  },
};

const mutations = {
  SET_CUSTOMERS_LIST(state, incomingData) {
    state.customersList = incomingData;
  },
  SET_CUSTOMER(state, customer) {
    state.selectedCustomer = customer;
  },
  SET_SHORT_CODE(state, shortCode) {
    state.selectedShortCode = shortCode;
  },
  SET_CUSTOMER_PRESETS(state, data) {
    state.customerPresets = data;
  },
  SET_PRESETS_LOADING(state, bool) {
    state.presetsLoading = bool;
  },
  SET_TITLE_OFFICER(state, titleOfficer) {
    state.selectedTitleOfficer = titleOfficer;
  },
  SET_STATE(state, selectedState) {
    state.selectedState = selectedState;
  },
  SET_COUNTY(state, county) {
    state.selectedCounty = county;
  },
  SET_TRANS_TYPE(state, transType) {
    state.selectedTransType = transType;
  },
  SET_ORDER_TYPE(state, orderType) {
    state.selectedOrderType = orderType;
  },
  SET_STICKER_INPUT(state, inputId) {
    state.stickerInput = inputId;
  },
  UPDATE_MANUAL_STICKER_BOOL(state, bool) {
    state.isManualStickerInput = bool;
  },
  UPDATE_STICKER_INPUT(state, val) {
    state.stickerInput = val;
  },
  UPDATE_STICKER_VALIDATION(state, bool) {
    state.isStickerValid = bool;
  },
  UPDATE_STICKER_MSG(state, msg) {
    state.stickerMsg = msg;
  },
  UPDATE_SHOW_STICKER_MSG(state, bool) {
    state.showStickerMsg = bool;
  },
  SET_ENUM(state, selectedEnum) {
    state.selectedEnum = selectedEnum;
  },
  SET_DOCTYPES(state, data) {
    state.doctypes = data;
  },
  SET_STATUS(state, status) {
    state.status = status;
  },
  CLEAR_DOCTYPES(state) {
    state.manifestRows = state.manifestRows.map((row, i) => {
      return {
        ...row,
        documentType: "",
        documentTypeID: "",
        selectedDoctypeObj: "",
      };
    });
  },
  UPDATE_MANIFEST_ROW(state, rowObj) {
    state.manifestRows = state.manifestRows.map((row, i) => {
      if (i == rowObj.rowIndex) return { ...row, ...rowObj };
      return row;
    });
  },

  UPDATE_MANIFEST_ROWS(state, rows) {
    state.manifestRows = rows;
  },

  ADD_MANIFEST_ROW(state) {
    const lastId = state.manifestRows[state.manifestRows.length - 1].id;
    const orderNumber =
      state.manifestRows[state.manifestRows.length - 1].orderNumber;
    const len = state.manifestRows.length;
    state.manifestRows.push({
      id: lastId + 1,
      orderNumber,
      conformed: false,
      certified: false,
      documentType: null,
      documentTypeID: null,
      estFee: 0, // recording fee
      estCountyTax: 0, // county fee
      estCityTax: 0, // city fee
      estSB2Fee: 0,
      hasConformImage: false,
      documentTypeUE: null,
      selectedDoctypeObj: null,
      manifestID: null,
      processingOrder: null,
      currentStatusID: null,
      notes: "",
      touched: false,
      rowIndex: len,
    });
  },

  SET_HEAD_DATA(state, data) {
    state.headData = data;
  },

  SET_PAYLOAD_ID(state, payloadId) {
    state.payloadID = payloadId;
  },

  SET_MANIFEST_ID(state, manifestId) {
    state.manifestID = manifestId;
  },

  RESET_CUSTOMER_SELECTIONS(state) {
    state.customerPresets = {};
    state.selectedShortCode = "";
    state.selectedTitleOfficer = "";
    state.selectedState = "";
    state.selectedCounty = "";
    state.selectedTransType = "";
    state.selectedOrderType = "";
    state.selectedEnum = "";
    state.recordingTime = "";
    state.doctypes = [];
    state.pdfUrl = "";
    state.payloadID = null;
    state.status = "";
  },

  RESET_MANIFEST_ROWS(state) {
    state.manifestRows = [
      {
        id: 1,
        orderNumber: null,
        conformed: false,
        certified: false,
        documentType: null,
        documentTypeID: null,
        estFee: 0, // recording fee
        estCountyTax: 0, // county fee?
        estCityTax: 0, // city fee?
        estSB2Fee: 0,
        documentTypeUE: null,
        selectedDoctypeObj: null,
        manifestID: null,
        processingOrder: null,
        currentStatusID: null,
        notes: "",
        touched: false,
        rowIndex: 0,
      },
    ];
  },

  SET_PDF_URL(state, pdfUrl) {
    state.pdfUrl = pdfUrl;
  },

  SET_RECORDING_DATE(state, recordingDate) {
    state.recordingDate = recordingDate;
  },

  SET_IS_EXISTING_MANIFEST(state, bool) {
    state.isExistingManifest = bool;
  },

  RESET_ROW_ORDER_IDS(state) {
    let idTracker = 0;
    state.manifestRows = state.manifestRows.map((obj, i) => {
      if (!obj.delete) {
        idTracker++;
        return {
          ...obj,
          rowIndex: i,
          id: idTracker,
        };
      } else {
        return {
          ...obj,
          rowIndex: i,
          id: "",
        };
      }
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
