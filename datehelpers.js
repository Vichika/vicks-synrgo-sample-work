/**
 * Used to get the month day of date (e.g., 11-23)
 * @param {Date} dateObj
 * @return {String}
 */

export const extractMonthDay = (dateObj) => {
  let month = dateObj.getMonth();
  let date = dateObj.getDate();
  // account for zero based indexing with months
  month += 1;
  month = month.toString();
  date = date.toString();
  return `${month}-${date}`;
};

/**
 * Determine the Federal holidays for the current year
 * @return {Array} Strings (e.g., ['1-1', '7-4', '12-25'...])
 */

export const getHolidays = () => {
  let today = new Date();
  let currentYear = today.getFullYear();
  /**
   * Need to determine Thanksgiving for current year.
   * Get current year, determine 4th Thurs in Nov
   */
  let thanksgiving;
  let blackFriday;
  let november = new Date();
  november.setMonth(10);
  november.setFullYear(currentYear);
  let thursdayCount = 0;
  for (let i = 1; i < 31; i++) {
    november.setDate(i);
    let currentDay = november.getDay();
    if (currentDay == 4) {
      thursdayCount++;
      if (thursdayCount == 4) {
        thanksgiving = november;
        break;
      }
    }
  }
  /**
   * Memorial Day is the last Monday in May
   */
  let memorialDay;
  let may = new Date();
  may.setMonth(4);
  may.setFullYear(currentYear);
  for (let i = 31; i > 23; i--) {
    may.setDate(i);
    let currentDay = may.getDay();
    if (currentDay == 1) {
      memorialDay = may;
      break;
    }
  }
  /**
   * Labor Day is the first Monday of September
   */
  let laborDay;
  let september = new Date();
  september.setMonth(8);
  september.setFullYear(currentYear);
  for (let i = 1; i < 8; i++) {
    september.setDate(i);
    let currentDay = september.getDay();
    if (currentDay == 1) {
      laborDay = september;
      break;
    }
  }

  /**
   * MLK day, third Monday in January
   */
  let MLKday;
  let january = new Date();
  january.setMonth(0);
  january.setFullYear(currentYear);
  for (let i = 15; i < 22; i++) {
    january.setDate(i);
    let currentDay = january.getDay();
    if (currentDay == 1) {
      MLKday = january;
      break;
    }
  }

  /**
   * President's day, third Monday in February
   */
  let prezDay;
  let february = new Date();
  february.setMonth(1);
  february.setFullYear(currentYear);
  for (let i = 15; i < 22; i++) {
    february.setDate(i);
    let currentDay = february.getDay();
    if (currentDay == 1) {
      prezDay = february;
      break;
    }
  }
  laborDay = extractMonthDay(laborDay);
  thanksgiving = extractMonthDay(thanksgiving);
  blackFriday = String(Number(thanksgiving.split("-")[1]) + 1);
  blackFriday = `11-${blackFriday}`;
  memorialDay = extractMonthDay(memorialDay);
  MLKday = extractMonthDay(MLKday);
  prezDay = extractMonthDay(prezDay);

  const holidays = [
    "1-1",
    MLKday,
    prezDay,
    memorialDay,
    "7-4",
    laborDay,
    "11-11",
    thanksgiving,
    blackFriday,
    "12-25",
  ];

  return holidays;
};
/**
 * @return Array of Date objects that correspond to the
 * array of strings provided in getHolidays
 */
export const getHolidayDates = () => {
  const today = new Date();
  const currentYear = today.getFullYear();
  let holidays = getHolidays();
  holidays = holidays.map((date) => date.split("-"));
  let dates = [];
  for (let date of holidays) {
    const currentMonth = parseInt(date[0]) - 1;
    const currentDay = parseInt(date[1]);
    const currentHoliday = new Date(currentYear, currentMonth, currentDay);
    dates.push(currentHoliday);
  }
  return dates;
};

/**
 * Need to account for weekend dates between business start and end range.
 */
export const getWeekendDates = (start, end) => {
  let weekends = [];
  let d = new Date(start);
  while (d < end) {
    let newDate = d.setDate(d.getDate() + 1);
    d = new Date(newDate);
    let day = d.getDay();
    if (day == 0 || day == 6) {
      weekends.push(d);
    }
    d = new Date(newDate);
  }
  return weekends;
};

/**
 * Determine if the Date passed in is a business day
 * @param {Date} dateObj
 * @return {Boolean}
 */
export const isBusinessDay = (dateObj) => {
  let day = dateObj.getDay();
  if (day == 0 || day == 6) {
    return false;
  } else {
    // Monday - Friday, check if falls on holiday
    const holidays = getHolidays();
    let monthDay = extractMonthDay(dateObj);
    if (holidays.includes(monthDay)) {
      return false;
    }
  }
  return true;
};

/**
 * Need to determine the next business day and return a Date object
 * from current day
 * @return {Date}
 *
 */

export const getNextBizDay = () => {
  let nextDay = new Date();
  for (let i = 1; i < 6; i++) {
    // reference https://stackoverflow.com/questions/23081158/javascript-get-date-of-the-next-day
    nextDay.setDate(nextDay.getDate() + 1);
    if (isBusinessDay(nextDay)) {
      return nextDay;
    }
  }
};

export const getPrevBizDay = () => {
  let prevDay = new Date();
  for (let i = 6; i > 1; i--) {
    prevDay.setDate(prevDay.getDate() - 1);
    if (isBusinessDay(prevDay)) {
      return prevDay;
    }
  }
};

export const getValidBizDateRange = () => {
  let start, end;
  const today = new Date();
  if (isBusinessDay(today)) {
    start = today;
    end = getNextBizDay();
    return {
      start,
      end,
    };
  } else {
    /**
     * If it's a weekend or holiday, the next business
     * day should be the only option
     */
    start = getNextBizDay();
    end = getNextBizDay();
    return {
      start,
      end,
    };
  }
};

/**
 *
 * Seems that using new Date('2019-06-20') is unreliable and
 * sometimes returns a date object with referencing the previous
 * day i.e 2019-06-19, this is suppose to return correct Date Object
 * @param {String} - Format must be '2019-06-20'
 * @return {Date}
 *
 */

export const correctlyConvertDateString = (str) => {
  const year = parseInt(str.slice(0, 4));
  // 0-based months
  const month = parseInt(str.slice(5, 7)) - 1;
  const date = parseInt(str.slice(8, 10));
  const finalDate = new Date();
  finalDate.setFullYear(year);
  finalDate.setMonth(month, date);
  return finalDate;
};

/**
 * Calls Date Object methods to return Year:Month:Date (e.g., 1999-12-31)
 * @param {Date} [dateToFormat = new Date()]
 *        - will default to current date if no param is passed
 * @return {String}
 */

export const getFormattedDate = (dateToFormat = new Date()) => {
  const year = dateToFormat.getFullYear();
  // Zero based, (i.e., January returns 0), so add 1
  let month = dateToFormat.getMonth() + 1;

  if (month < 10) {
    month = "0" + month.toString();
  }
  let date = dateToFormat.getDate();

  if (date < 10) {
    date = "0" + date.toString();
  }

  return `${year}-${month}-${date}`;
};

export const millisToMinutesAndSeconds = (millis) => {
  const minutes = Math.floor(millis / 60000);
  let seconds = ((millis % 60000) / 1000).toFixed(0);
  seconds = seconds < 10 ? `0${seconds}` : seconds;
  return `${minutes}:${seconds}`;
};

/**
 *
 * @param {String} timeStamp
 * @return {String} milliseconds 30 minutes added to timeStamp param
 */

export const thirtyMinsLater = (timeStamp) => {
  let time = timeStamp.slice(11);
  let date = timeStamp.slice(0, 10);
  let hour = parseInt(time.slice(0, 2));
  let minutes = parseInt(time.slice(3, 5));
  let seconds = time.slice(6);
  if (minutes <= 29) {
    minutes += 30;
  } else {
    if (hour == 24) {
      hour = 1;
    } else {
      hour += 1;
    }

    minutes = minutes - 30;
  }
  let timeStampPlus30Min = new Date(`${date} ${hour}:${minutes}:${seconds}`);
  return timeStampPlus30Min.getTime();
};
